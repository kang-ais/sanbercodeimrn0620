import React, { useState } from 'react';
import { MaterialIcons } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image
} from 'react-native';

export default function RegisterScreen() {

    return (
        <View style={styles.container}>
            <Image source={require('./images/logo-sanber.png')} style={{ width: 300, height: 100, marginTop: -100 }} />
            <Text style={{ marginTop: 15, fontSize: 25 }}>Register</Text>
            <TextInput style= {styles.inputText} placeholder="Username" />
            <TextInput style= {styles.inputText} placeholder="Email" />
            <TextInput style= {styles.inputText} placeholder="Password" />
            <TextInput style= {styles.inputText} placeholder="Ulangi Password" />
            
            <TouchableOpacity style={styles.buttonMasuk}>
                <Text style={{ backgroundColor: "#003366", color: "white", paddingHorizontal: 25, paddingVertical: 5, borderRadius: 200 }}>Daftar </Text>
            </TouchableOpacity>
            <View style={{ marginTop: 15 }}>
                <Text>Atau</Text>
            </View>
            <TouchableOpacity style={styles.buttonDaftar}>
                <Text style={{ backgroundColor: "#3EC6FF", color: "white", paddingHorizontal: 25, paddingVertical: 5, borderRadius: 200 }}>Masuk ?</Text>
            </TouchableOpacity>
        </View>

    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
    },
    text: {
        fontSize: 30, color: '#04063c'
    },
    inputText: {
        marginTop: 20,
        width: 200,
        height: 30,
        paddingHorizontal: 10,
        borderBottomWidth: 1,
        borderColor: "#003366",
        flexDirection: "row"
    },
    buttonMasuk: {
        marginTop: 20,
        height: 30,
        alignItems: 'center',
    },
    buttonDaftar: {
        marginTop: 15,
        height: 30,
        alignItems: 'center',
    }
});
