import React from 'react';
import { MaterialIcons } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image
} from 'react-native';

export default function AboutScreen() {

    return (
        <View style={styles.container}>
            <Text style={{ marginTop: 15, fontSize: 30, color: "#003366", marginTop: 0, fontWeight: "bold" }}>Tentang Saya </Text>
            <Image source={require('./images/ais-pp.png')} style={{ width: 100, height: 100, borderRadius: 300, marginTop: 10 }} />
            <Text style={{ marginTop: 15, fontSize: 25, color: "#003366", fontWeight: 'bold' }}>AIS - Ahmad Iman Sudrajat</Text>
            <Text style={{ marginTop: 5, fontSize: 15, color: "#65b7f3", fontWeight: 'bold' }}>React Native Developer</Text>
            <View style={styles.boxPorto}>
                <Text style={{ borderBottomWidth: 1, paddingLeft: 5 }}>Portofolio</Text>
                <View style={styles.listPorto}>
                    <View style={styles.detilPorto}>
                        <AntDesign name="github" size={34} color="#65b7f3" />
                        <Text style={{ marginLeft: 10 }}>kang-ais</Text>
                    </View>
                    <View style={styles.detilPorto}>
                        <AntDesign name="gitlab" size={34} color="#65b7f3" />
                        <Text style={{ marginLeft: 10 }}>kang-ais</Text>
                    </View>
                </View>
            </View>
            <View style={styles.boxKontak}>
                <Text style={{ borderBottomWidth: 1, paddingLeft: 5, marginBottom: 5 }}>Hubungi Saya</Text>
                <View style={styles.listKontak}>
                    <View style={styles.detilKontak}>
                        <FontAwesome name="facebook-square" size={34} color="#65b7f3" />
                        <Text style={{ marginLeft: 10 }}>kang.ais</Text>
                    </View>
                    <View style={styles.detilKontak}>
                        <AntDesign name="instagram" size={34} color="#65b7f3" />
                        <Text style={{ marginLeft: 10 }}>@_kang_ais</Text>
                    </View>
                    <View style={styles.detilKontak}>
                        <AntDesign name="twitter" size={34} color="#65b7f3" />
                        <Text style={{ marginLeft: 10 }}>@_kang_ais</Text>
                    </View>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
    },
    boxPorto: {
        display: 'flex',
        backgroundColor: "#EFEFEF",
        height: 100,
        width: 320,
        marginBottom: 10,
        borderRadius: 10,
        marginTop: 10
    },
    boxKontak: {
        display: 'flex',
        backgroundColor: "#EFEFEF",
        height: 160,
        width: 320,
        borderRadius: 20
    },
    listKontak: {
        display: 'flex',
        alignItems: 'center',
    },
    detilKontak: {
        display: 'flex',
        flexDirection: 'row',
        marginVertical: 5
    },
    detilPorto: {
        display: 'flex',
        alignItems: 'center',
        marginTop: 10
    },
    listPorto: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    }
});
