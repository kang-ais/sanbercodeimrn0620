// Soal No. 1 (Range)

function range(startNum,finishNum) {
    var numbers = [];
    if ((startNum == undefined) | (finishNum == undefined)) {
        return -1
    }
    else if (startNum < finishNum) {
        for(var angka = startNum; angka <= finishNum; angka++) {
           numbers.push(angka)
        }
        return numbers
    }  
    else{
        for(var angka = startNum; angka >= finishNum; angka--) {
            numbers.push(angka)
         }
         return numbers
    } 
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log('--------------------------------')

// Soal No. 2 (Range with Step)

function rangeWithStep(startNum,finishNum,step) {
    var numbers = [];
    if ((startNum == undefined) | (finishNum == undefined)) {
        return -1
    }
    else if (startNum < finishNum) {
        for(var angka = startNum; angka <= finishNum;) {
           numbers.push(angka)
           angka += step;
        }
        return numbers
    }  
    else{
        for(var angka = startNum; angka >= finishNum;) {
            numbers.push(angka)
            angka -= step;
         }
         return numbers
    } 
}


console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log('--------------------------------')

// Soal No. 3 (Sum of Range)
function sum(start,finish,step) {
    var numbers = [];
    var jumlah = 0;
    if (step == undefined) {
        step = 1
    }
    if ((start == undefined) | (finish == undefined)) {
        return -1
    }
    else if (start < finish) {
        for(var angka = start; angka <= finish;) {
           numbers.push(angka)
           jumlah += angka
           angka += step;
        }
        return jumlah
    }  
    else{
        for(var angka = start; angka >= finish;) {
            numbers.push(angka)
            jumlah += angka
            angka -= step;
         }
         return jumlah
    } 
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log('--------------------------------')

// Soal No. 4 (Array Multidimensi)
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function datahandling(data){
    for (var i = 0; i < data.length; i++) {

        // This loop is for inner-arrays
        for (var j = 0; j < data[i].length; j++) {
            if (j==0){
                console.log("Nomor ID: " + data[i].slice(j,j+1)) 
            }
            else if (j==1){
                console.log("Nama Lengkap: " + data[i].slice(j,j+1)) 
            }
            else if (j==2){
                console.log("TTL: " + data[i].slice(j,j+1)) 
            }
            else if (j==3){
                console.log("Hobi: " + data[i].slice(j,j+1)) 
            }
                       
        }
    }
}

console.log(datahandling(input))


console.log('--------------------------------')

// Soal No 5(Balik Kata)
var kata = []
function balikKata(string){    
    for (var i = 0; i < string.length; i++) {
        kata.unshift(string[i])
    }
    return kata
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log('--------------------------------')

// Soal No 6 (Metode Array)

var input1 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
var nama_bulan;
function dataHandling2(data){
    data.splice(1,1,"Roman Alamsyah Elsharawy")
    data.splice(2,1,"Provinsi Bandar Lampung")
    data.splice(4,1,"Pria","SMA Internasional Metro")
    console.log(data)

    tanggal = data[3].split("/")
    bulan = tanggal[1]
    if (bulan[0] == 0){
        bulan = bulan[1]
    }
 
    const bulan1 = parseInt(bulan, 10)

    switch(bulan1) {
        case 1:   { nama_bulan = "Januari"; break; }
        case 2:   { nama_bulan = "Februari"; break; }
        case 3:   { nama_bulan = "Maret"; break; }
        case 4:   { nama_bulan = "April"; break; }
        case 5:   { nama_bulan = "Mei"; break; }
        case 6:   { nama_bulan = "Juni"; break; }
        case 7:   { nama_bulan = "Juli"; break; }
        case 8:   { nama_bulan = "Agustus"; break; }
        case 9:   { nama_bulan = "September"; break; }
        case 10:   { nama_bulan = "Oktober"; break; }
        case 11:   { nama_bulan = "November"; break; }
        case 12:   { nama_bulan = "Desember"; break; }}
    
    console.log(nama_bulan)
    
    // Mengurutkan secara Descending
    
    tanggal.sort(function (value1, value2) { return value1 < value2 } ) ; 
    console.log(tanggal) // [12, 3, 1] 
    var slug = tanggal.join("-")
    console.log(slug) 
    var irisan1 = data.slice(1,2) 
    console.log(irisan1) 
}

dataHandling2(input1)
