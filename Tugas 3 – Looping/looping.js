// 1) Looping While
/*
LOOPING PERTAMA
2 - I love coding
4 - I love coding
6 - I love coding
8 - I love coding
10 - I love coding
12 - I love coding
14 - I love coding
16 - I love coding
18 - I love coding
20 - I love coding
*/
console.log('LOOPING PERTAMA');
var lop1 = 2;
while(lop1 < 22 ) {
    console.log(lop1 +' '+ '-'+' '+ "I love coding");
    lop1 +=2;
}

console.log(' ');

/*
LOOPING KEDUA
20 - I will become a mobile developer
18 - I will become a mobile developer                                                                              
16 - I will become a mobile developer
14 - I will become a mobile developer
12 - I will become a mobile developer
10 - I will become a mobile developer
8 - I will become a mobile developer
6 - I will become a mobile developer
4 - I will become a mobile developer
2 - I will become a mobile developer
*/
console.log('LOOPING KEDUA');
var lop2 = 20;
while(lop2 > 0) {
    console.log(lop2 +' '+ '-'+' '+ "I will become a mobile developer");
    lop2 -=2;
}


console.log(' ');


// 2) Looping for

/*
OUTPUT 
1 - Santai
2 - Berkualitas
3 - I Love Coding 
4 - Berkualitas
5 - Santai
6 - Berkualitas
7 - Santai
8 - Berkualitas
9 - I Love Coding
10 - Berkualitas
11 - Santai
12 - Berkualitas
13 - Santai
14 - Berkualitas
15 - I Love Coding
16 - Berkualitas
17 - Santai
18 - Berkualitas
19 - Santai
20 - Berkualitas
*/

for (var a = 1; a <= 20; a++) {
    if (a % 3 == 0 && a % 2 == 1) {
      console.log(a + " I Love Coding");
    } else if (a % 2 == 1) {
      console.log(a + " Santai");
    } else if (a % 2 == 0) {
      console.log(a + " Berkualitas");
    }
  }


console.log(' ');


// 3) Membuat Persegi Panjang #
/* 
Ouput
########
########
########
######## 
*/

for (var a = 1 ; a <=4 ; a++){      
    var pp = ''
    for (var b = 1 ; b <=8 ; b++){  
        pp += '#';   
    }
    console.log(pp);
}  



console.log(' ');


// 4) Membuat Tangga #
/* 
Ouput
#
##
###
####
#####
######
####### 
*/

var Tangga = "";
var a = 0;
while (a < 7){
    console.log(Tangga += "#");
    a++;
};


console.log(' ');


// 5) Membuat Papan Catur #
/* 
Ouput
 # # # #
# # # # 
 # # # #
# # # # 
 # # # #
# # # # 
 # # # #
# # # # 
*/

var Papan = ''
for (var a = 1 ; a <=8 ; a++){
    if (a>1) {
        Papan += '\n'
    }
    for (var b = 1 ; b <= 8 ; b++) { 
        if((a+b) % 2 == 0) {
            Papan += " ";
        }else{
            Papan += "#"; }
    }
}  
console.log(Papan);
