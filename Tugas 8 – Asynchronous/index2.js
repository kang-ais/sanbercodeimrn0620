// Masih di folder yang sama dengan promise.js, buatlah sebuah file dengan nama index2.js. Tuliskan code sebagai berikut

var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

// Lanjutkan code untuk menjalankan function readBooksPromise 
// Lakukan hal yang sama dengan soal no.1, habiskan waktu selama 10000 ms (10 detik) untuk membaca semua buku dalam array books.!

readBooksPromise(10000, books[0])
.then(sisaWaktu => { 
    return readBooksPromise(sisaWaktu, books[1])
})
.then(sisaWaktu => {
    return readBooksPromise(sisaWaktu,books[2])
})
.then(sisaWaktu => {
    return readBooksPromise(sisaWaktu,books[0])
})
.catch(function (error) {
    console.log(error);
})
