/*
SOAL if-else ======================================================
*/

var nama = "John"
var peran = ""

// Output untuk Input nama = '' dan peran = ''
//"Nama harus diisi!"

var nama = ""
var peran = ""
if( nama == "" || peran == "") {
    console.log ("Nama harus diisi!")
}


//Output untuk Input nama = 'John' dan peran = ''
//"Halo John, Pilih peranmu untuk memulai game!"
var nama = "John"
var peran = ""
if (nama == "" || peran == "") {
    console.log("Halo John, Pilih peranmu untuk memulai game!")
} else {(nama == "John" || peran == "")
    console.log("Nama harus diisi!")
}


//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
//"Selamat datang di Dunia Werewolf, Jane"
//"Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!"
var nama = "Jane"
var peran = "Penyihir"
if (nama == "Jane" || peran == "Penyihir") {
    console.log("Selamat datang di Dunia Werewolf, Jane" + "\n" +
    "Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
} else if (nama == "Jane" || peran == "") {
    console.log("Halo Jane, Pilih peranmu untuk memulai game!")
} else {
    (nama == "" || peran == "")
    console.log("Nama harus diisi!" )
}


//Output untuk Input nama = 'Jenita' dan peran 'Guard'
//"Selamat datang di Dunia Werewolf, Jenita"
//"Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf."
var nama = "Jenita"
var peran = "Guard"
if (nama == "Jenita" || peran == "Guard") {
    console.log("Selamat datang di Dunia Werewolf, Jenita" + "\n" +
    "Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama == "Jenita" || peran == "") {
    console.log("Halo Jenita, Pilih peranmu untuk memulai game!")
} else {
    (nama == "" || peran == "")
    console.log("Nama harus diisi!" )
}



//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
//"Selamat datang di Dunia Werewolf, Junaedi"
//"Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!" 
var nama = "Junaedi"
var peran = "Warewolf"
if (nama == "Junaedi" || peran == "Warewolf") {
    console.log("Selamat datang di Dunia Werewolf, Junaedi" + "\n" +
    "Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")
} else if (nama == "Junaedi" || peran == "") {
    console.log("Halo Junaedi, Pilih peranmu untuk memulai game!")
} else {
    (nama == "" || peran == "")
    console.log("Nama harus diisi!" )
}


/*
SOAL switch-case ==========================================================
*/

var hari = 21; 
var bulan = 1; 
var tahun = 1945;

switch(bulan){
    case 1: bulan = "Januari"; break;
    case 2: bulan = "Februari"; break;
    case 3: bulan = "Maret"; break; 
    case 4: bulan = "April"; break; 
    case 5: bulan = "Mei"; break; 
    case 6: bulan = "Juni"; break;
    case 7: bulan = "Juli"; break;
    case 8: bulan = "Agustus"; break;
    case 9: bulan = "September"; break;
    case 10: bulan = "Oktober"; break;
    case 11: bulan = "November"; break;
    case 12: bulan = "Desember"; break;
    default: bulan = "Nama Bulan";
}
console.log(hari + " " + bulan + " " + tahun);

//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 
/*
var tanggal; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

switch(hari) { 
    case 1: hari = 1; break;
    case 2: hari = 2; break;
}
gak perlu dibuat karena sama-sama angka, sama dn var tahun

*/

